### Download the dataset
* Download the ["Amazon fine food reviews"](https://www.kaggle.com/snap/amazon-fine-food-reviews/downloads/amazon-fine-food-reviews.zip/2) dataset
* Extract a file "Reviews.csv" into a folder called "amazon-fine-food-reviews" so that all reviews are in this path 
```
amazon-fine-food-reviews/Reviews.csv
``` 

### Build this example as a jar
```
mvn package
```

### Run WordCount_Seq
```
java -cp target/basic_word_count-1.0-SNAPSHOT.jar edu.rit.cs.basic_word_count.WordCount_Seq
```
### Issues:
* Out of Heap Space error, fixed by using sub lists to decrease memory overhead

### Differences between WordCount_Seq & SubtaskOne:
```
In SubtaskOne, the hashmap used to store the tolkenized words is replaced with a tree map. This way the output is
automatically sorted.
```

### Differences between SubtaskOne & SubtaskTwo:
```
In SubtaskTwo, the data set is segmented using the sublist function. Then, each segment is spun off as a thread.
Each thread inidividually sorts its sublist of the data set and all are merged into one.

Implementation note: a synchonized list is instantiated and passed into the thread constructors by reference. The before
the thread finishes, it adds the tree map it builds to the list.
```

### Differences between SubtaskTwo & SubtaskThree:
```
In SubtaskThree, the main class is converted into a server and the thread class into a client.
Implementation note: the connection constructor was modified to accept the list subset that the client was to process.

### Run WordCount_Seq_Improved
```
java -cp target/basic_word_count-1.0-SNAPSHOT.jar edu.rit.cs.basic_word_count.WordCount_Seq_Improved
```

### Issues
- “Exception in thread “main” java.lang.OutOfMemoryError: Java heap space” error. One naive solution is increase the memory. But, you need to think about how to optimize the program, so that you can avoid this naive fix.
```
export JVM_ARGS="-Xmx1024m -XX:MaxPermSize=256m"
```