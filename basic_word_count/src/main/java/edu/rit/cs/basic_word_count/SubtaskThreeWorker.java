package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubtaskThreeWorker {

    public static void main(String args[]) {
        List<AmazonFineFoodReview> wordList;

        // arguments supply message and hostname of destination
        String server_address = args[0];

        Socket s = null;
        try {
            // Instantiate socket & streams
            int serverPort = 7896;
            s = new Socket(server_address, serverPort);
            ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(s.getInputStream());

            wordList = (List<AmazonFineFoodReview>) in.readObject();

            /* Tokenize words */
            List<String> words = new ArrayList<String>();
            for(AmazonFineFoodReview review : wordList) {
                Pattern pattern = Pattern.compile("([a-zA-Z]+)");
                Matcher matcher = pattern.matcher(review.get_Summary());

                while(matcher.find())
                    words.add(matcher.group().toLowerCase());
            }

//        /* Count words */
            TreeMap<String, Integer> wordcount = new TreeMap<>();
            for(String word : words) {
                if(!wordcount.containsKey(word)) {
                    wordcount.put(word, 1);
                } else{
                    int init_value = wordcount.get(word);
                    wordcount.replace(word, init_value, init_value + 1);
                }
            }

            /* Send back */
            out.writeObject(wordcount);

        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (s != null)
                try {
                    s.close();
                } catch (IOException e) {
                    System.out.println("close:" + e.getMessage());
                }
        }
    }
}

