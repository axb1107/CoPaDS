package edu.rit.cs.basic_word_count;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SortThread implements Runnable {

    private List<AmazonFineFoodReview> wordList;
    private List<TreeMap<String, Integer>> treeMapList;

    public SortThread(List<AmazonFineFoodReview> wordList, List<TreeMap<String, Integer>> treeMapList) {
        this.wordList = wordList;
        this.treeMapList = treeMapList;
    }

    public void run() {
        try {
            /* Tokenize words */
            List<String> words = new ArrayList<String>();
            for(AmazonFineFoodReview review : wordList) {
                Pattern pattern = Pattern.compile("([a-zA-Z]+)");
                Matcher matcher = pattern.matcher(review.get_Summary());

                while(matcher.find())
                    words.add(matcher.group().toLowerCase());
            }

            /* Count words */
            TreeMap<String, Integer> wordcount = new TreeMap<>();
            for(String word : words) {
                if(!wordcount.containsKey(word)) {
                    wordcount.put(word, 1);
                } else{
                    int init_value = wordcount.get(word);
                    wordcount.replace(word, init_value, init_value + 1);
                }
            }
            // Add tree map to list, which is passed in by reference and outside of the scope of this class
            treeMapList.add(wordcount);

        } catch (Exception e) {
            System.out.println("Something");
        }
    }

}
