package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubtaskTwo {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file = "amazon-fine-food-reviews/Reviews.csv";

    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {

        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    public static void main(String[] args) {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        // List of lists in which to segment reviews
        List<List<AmazonFineFoodReview>> masterList = new ArrayList<>();

        // Segment into numPartitions lists, the 5th list will have < 5 elements more than the others
        int numPartitions = 5;
        int numReviews = allReviews.size();

        for(int i = 0; i < numPartitions; i++) {
            if(i < numPartitions - 1) {
                masterList.add(allReviews.subList((int)((double)i * numReviews/numPartitions),
                        (int)((i + 1.0) * numReviews/numPartitions) - 1));
            } else {
                masterList.add(allReviews.subList((i * numReviews/numPartitions), numReviews));
            }
            System.out.println(masterList.get(i).size());
        }

            /* For debug purpose */
//        for(AmazonFineFoodReview review : allReviews){
//            System.out.println(review.get_Text());
//        }
        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();

        List<TreeMap<String, Integer>> treeMapList =
                Collections.synchronizedList(new ArrayList<TreeMap<String, Integer>>());

        List<Thread> threadList = new ArrayList<>();

        // Start threads
        for(List<AmazonFineFoodReview> reviewList : masterList) {
            Thread sortThread = new Thread(new SortThread(reviewList, treeMapList));
            sortThread.start();
            threadList.add(sortThread);
        }

        // Ensure threads finish running before merging
        for(Thread thread : threadList) {
            try {
                thread.join();
            } catch (Exception e) {
                System.out.println("Exception caught by " + e);
            }
        }

        // Merge all of the tree maps together
        TreeMap<String, Integer> wordsTreeMap = treeMapList.get(0);
        for(int i = 1; i < treeMapList.size(); i++) {
            TreeMap<String, Integer> thisTreeMap = treeMapList.get(i);
            for(String key : thisTreeMap.keySet()) {
                if(wordsTreeMap.containsKey(key)) {
                    wordsTreeMap.replace(key, wordsTreeMap.get(key) + thisTreeMap.get(key));
                } else {
                    wordsTreeMap.put(key, thisTreeMap.get(key));
                }
            }
        }

        myTimer.stop_timer();

        print_word_count(wordsTreeMap);
        System.out.println("\nWordcount: " + wordsTreeMap.size());

        myTimer.print_elapsed_time();
    }

}
