package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class SubtaskThreeMaster {

    public static final String AMAZON_FINE_FOOD_REVIEWS_file = "amazon-fine-food-reviews/Reviews.csv";

    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {

        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }


    public static void main(String [] args) {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        SubtaskThreeMaster tcp = new SubtaskThreeMaster();

        // Segment reviews into given number of partitions
        int numPartitions = 5;
        int numReviews = allReviews.size();

        // List containing segments of the input
        List<List<AmazonFineFoodReview>> masterList = new ArrayList<>();

        // List to hold tree maps once each segment of the input is sorted
        List<TreeMap<String, Integer>> treeMapsList = new ArrayList<>();

        // Segment dataset into lists and add to masterList
        for(int i = 0; i < numPartitions; i++) {
            List sublist = new ArrayList();
            if(i < numPartitions - 1) {
                sublist.addAll(allReviews.subList((i * numReviews/numPartitions),
                        ((i + 1) * numReviews/numPartitions) - 1));
            } else {
                sublist.addAll(allReviews.subList((i * numReviews/numPartitions), numReviews));
            }
            masterList.add(sublist);
        }

        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();

        // Lists to hold connection objects so that we can access the tree maps
        List<Connection> connectionsList = new ArrayList<>();

        try {
            int serverPort = 7896;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("TCP Server is running and accepting client connections...");

            // Establish connections between client and server for each node to be created, give node sublist
            for(List<AmazonFineFoodReview> sublist : masterList) {
                Socket clientSocket = listenSocket.accept();
                Connection c = new Connection(clientSocket, sublist);
                connectionsList.add(c);
                treeMapsList.add(c.getTreeMap());
            }

            // Ensure that nodes have finished operating on their data sets before continuing with program
            boolean connectionAlive = true;
            while(connectionAlive) {
                for(Connection connection : connectionsList) {
                    if (connection.isAlive()) {
                        connectionAlive = true;
                        break;
                    } else {
                        connectionAlive = false;
                    }
                }
            }

            // Merge all outputs from worker nodes together
            TreeMap<String, Integer> wordsTreeMap = connectionsList.get(0).getTreeMap();
            for(int i = 1; i < treeMapsList.size(); i++) {
                TreeMap<String, Integer> thisTreeMap = connectionsList.get(i).getTreeMap();
                for(String key : thisTreeMap.keySet()) {
                    if(wordsTreeMap.containsKey(key)) {
                        wordsTreeMap.replace(key, wordsTreeMap.get(key) + thisTreeMap.get(key));
                    } else {
                        wordsTreeMap.put(key, thisTreeMap.get(key));
                    }
                }
            }

            myTimer.stop_timer();

            print_word_count(wordsTreeMap);
            System.out.println("\nWordcount: " + wordsTreeMap.size());
            myTimer.print_elapsed_time();

        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
    }
}

class Connection extends Thread {
    ObjectInputStream in;
    ObjectOutputStream out;
    Socket clientSocket;
    List<AmazonFineFoodReview> listSubset;
    TreeMap<String, Integer> wordcount;

    // Instantiate socket with client whose port is open and give it the data set
    public Connection(Socket aClientSocket, List<AmazonFineFoodReview> listSubset) {
        try {
            this.listSubset = listSubset;
            clientSocket = aClientSocket;
            in = new ObjectInputStream(clientSocket.getInputStream());
            out = new ObjectOutputStream(clientSocket.getOutputStream());
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    // Starts data exchange and execution
    public void run() {
        try {   // an echo server
            out.writeObject(listSubset);
            wordcount = (TreeMap<String, Integer>)in.readObject();
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Class:" + e.getMessage());
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {/*close failed*/}
        }
    }

    public TreeMap<String, Integer> getTreeMap() {
        return wordcount;
    }
}
